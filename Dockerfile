FROM ubuntu

#ADD ./entrypoint.sh /bin/entrypoint.sh

RUN mkdir -p /ffmpeg_sources /bin

RUN apt-get update  && apt-get -y install \
  git

RUN apt-get -y install \
  autoconf \
  automake \
  build-essential \
  cmake \
  git-core \
  libass-dev \
  libfreetype6-dev \
  libsdl2-dev \
  libtool \
  libva-dev \
  libvdpau-dev \
  libvorbis-dev \
  libxcb1-dev \
  libxcb-shm0-dev \
  libxcb-xfixes0-dev \
  pkg-config \
  texinfo \
  wget \
  zlib1g-dev \
  nasm \
  yasm \
  libx264-dev \
  libx265-dev \
  libnuma-dev \
  libvpx-dev \
  libfdk-aac-dev \
  libmp3lame-dev \
  libopus-dev

RUN cd /ffmpeg_sources && \
    git -C aom pull 2> /dev/null || git clone --depth 1 https://aomedia.googlesource.com/aom && \
    mkdir -p aom_build && \
    cd aom_build && \
    PATH="/bin:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="/ffmpeg_build" -DENABLE_SHARED=off -DENABLE_NASM=on ../aom && \
    PATH="/bin:$PATH" make && \
    make install

RUN wget -O ffmpeg-3.4.6.tar.bz2 https://ffmpeg.org/releases/ffmpeg-3.4.6.tar.bz2 && \
    tar xjvf ffmpeg-3.4.6.tar.bz2 && \
    mv /ffmpeg-3.4.6 /ffmpeg && \
    cd /ffmpeg && \
    PATH="/bin:$PATH" PKG_CONFIG_PATH="/ffmpeg_build/lib/pkgconfig" ./configure \
      --prefix="/ffmpeg_build" \
      --pkg-config-flags="--static" \
      --extra-cflags="-I/ffmpeg_build/include" \
      --extra-ldflags="-L/ffmpeg_build/lib" \
      --extra-libs="-lpthread -lm" \
      --bindir="/bin" \
      --enable-gpl \
      --enable-libass \
      --enable-libfdk-aac \
      --enable-libfreetype \
      --enable-libmp3lame \
      --enable-libopus \
      --enable-libvorbis \
      --enable-libvpx \
      --enable-libx264 \
      --enable-libx265 \
      --enable-nonfree && \
    PATH="/bin:$PATH" make && \
    make install && \
    hash -r

RUN echo "$VERSION"

RUN apt-get -y install \
    python3.7 \
    python3-pip

RUN mkdir /src
WORKDIR /src

COPY config.py /src/config.py
COPY requirements.pip /src/requirements.pip
COPY ffserver.conf /src/ffserver.conf

RUN python3.7 -m pip install -r requirements.pip
